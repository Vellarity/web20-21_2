# -*- coding: utf-8 -*-
"""
Задание 6.2

1. Запросить у пользователя ввод IP-адреса в формате 10.0.1.1
2. В зависимости от типа адреса (описаны ниже), вывести на стандартный поток вывода:
   'unicast' - если первый байт в диапазоне 1-223
   'multicast' - если первый байт в диапазоне 224-239
   'local broadcast' - если IP-адрес равен 255.255.255.255
   'unassigned' - если IP-адрес равен 0.0.0.0
   'unused' - во всех остальных случаях


Ограничение: Все задания надо выполнять используя только пройденные темы.
"""

ip_in = input('Введите IP адресс: ')
ip_sp = ip_in.split('.')

if int(ip_sp[0]) in range(1,224):
    print('unicast')
elif int(ip_sp[0]) in range(224,240):
    print('multiicast')
elif ip_in == '255.255.255.255':
    print('local broadcast')
elif ip_in == '0.0.0.0':
    print('unassigned')
else:
    print('unused')
