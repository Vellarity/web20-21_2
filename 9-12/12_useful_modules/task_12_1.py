# -*- coding: utf-8 -*
"""
Задание 12.1

Создать функцию ping_ip_addresses, которая проверяет пингуются ли IP-адреса.

Функция ожидает как аргумент список IP-адресов.

Функция должна возвращать кортеж с двумя списками:
* список доступных IP-адресов
* список недоступных IP-адресов

Для проверки доступности IP-адреса, используйте команду ping.

Ограничение: Все задания надо выполнять используя только пройденные темы.
"""

import subprocess 

listping = ['8.8.4.4', '1.1.1.1', '192.168.0.1','200.200.200.200']

def ping_ip_addresses(list_of_ips):
    can_ping = []
    cant_ping = []

    for line in list_of_ips:

        command = subprocess.run("ping {}".format(line),shell=True,stdout=subprocess.DEVNULL)
        if command.returncode == 0:
            can_ping.append(line)
        else:
            cant_ping.append(line)

    return can_ping, cant_ping


print(ping_ip_addresses(listping))
