# -*- coding: utf-8 -*-
"""
Задание 7.1

Обработать строки из файла ospf.txt и вывести информацию по каждой строке в таком виде:

Prefix                10.0.24.0/24
AD/Metric             110/41
Next-Hop              10.0.13.3
Last update           3d18h
Outbound Interface    FastEthernet0/0

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""

f = open("ospf.txt",'r')

namelist=['Prefix','AD/Metric','Next-Hop','Last update','Outbound Interface']

for line in f:
    line_sp = line.split()
    print("{:22} {:22}".format(namelist[0],line_sp[1]))
    print("{:22} {:22}".format(namelist[1],line_sp[2].strip('[]')))
    print("{:22} {:22}".format(namelist[2],line_sp[4].strip(',')))
    print("{:22} {:22}".format(namelist[3],line_sp[5].strip(',')))
    print("{:22} {:22}".format(namelist[4],line_sp[6].strip(',')))
    print("\n")

f.close()
    
