# -*- coding: utf-8 -*-
"""
Задание 6.2a

Сделать копию скрипта задания 6.2.

Добавить проверку введенного IP-адреса. Адрес считается корректно заданным, если он:
   - состоит из 4 чисел (а не букв или других символов)
   - числа разделенны точкой
   - каждое число в диапазоне от 0 до 255

Если адрес задан неправильно, выводить сообщение:
'Неправильный IP-адрес'

Сообщение должно выводиться только один раз.

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""

ip_in = input('Введите IP адресс: ')
ip_sp = ip_in.split('.')
count = 0
err = False

for i in ip_sp:
    count += 1
    

if count !=4:
    err = True
else:
    for i in ip_sp:
        if int(i) not in range(1,256):
            err = True

if err:
    print('Неправильный IP-адрес')    

elif int(ip_sp[0]) in range(1,224,1):
    print('unicast')
    
elif int(ip_sp[0]) in range(224,240,1):
    print('multiicast')
    
elif ip_in == '255.255.255.255':
    print('local broadcast')
    
elif ip_in == '0.0.0.0':
    print('unassigned')
    
else:
    print('unused')
