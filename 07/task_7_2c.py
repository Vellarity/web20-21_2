# -*- coding: utf-8 -*-
"""
Задание 7.2c

Переделать скрипт из задания 7.2b:
* передавать как аргументы скрипту:
 * имя исходного файла конфигурации
 * имя итогового файла конфигурации

Внутри, скрипт должен отфильтровать те строки, в исходном файле конфигурации,
в которых содержатся слова из списка ignore.
И записать остальные строки в итоговый файл.

Проверить работу скрипта на примере файла config_sw1.txt.

Ограничение: Все задания надо выполнять используя только пройденные темы.
"""

from sys import argv

ignore = ["duplex", "alias", "Current configuration"]

in_file = argv[1]
out_file = argv[2]

f = open(in_file,'r')

temp =''
deletelist =''

for line in f:
    if line.startswith('!')==False:
        temp += line
        if any(i in line for i in ignore):
            deletelist += line
            
s1 = temp.splitlines()
s2 = deletelist.splitlines()

s3 = [line for line in s1 if line not in s2]

f2 = open(out_file,'w')

f2.write('\n'.join(s3))

f.close()
f2.close()
