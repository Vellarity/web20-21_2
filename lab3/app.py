from flask import Flask, render_template, request, session, redirect, url_for, flash
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required
from mysql_db import MySQL

login_manager=LoginManager()

app = Flask(__name__)
application = app

app.config.from_pyfile('config.py')

mysql=MySQL(app)

login_manager.init_app(app)
login_manager.login_view='login'
login_manager.login_message='Для доступа к данной странице нужно аутенцифицироваться'
login_manager.login_message_category='warning'

def get_users():
    return [{'user_id':'1','login':'user','password':'qwerty'}]

#Для проверки пользователей Flask-Login требует добавления нескольких методов в класс User (get_id() и т.д.)
#Класс UserMixin предоставляет реализацию этих методов
class User(UserMixin):
    def __init__(self,user_id,login):
        super().__init__()
        self.id=user_id
        self.login=login

@login_manager.user_loader 
# Необходимо для загрузки пользователя. Дальнейшая функция принимает на вход параметры пользователя и возвращает объект пользователя, если пользователь существует
def load_user(user_id):
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM users WHERE id = %s;', (user_id,))
    db_user = cursor.fetchone()
    cursor.close()
    if db_user:
        return User(user_id = db_user.id, login = db_user.login)
    return None


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/visits')
def visits():
    if session.get('visits'):
        session['visits']+=1
    else:
        session['visits']=1
    return render_template('visits.html')


@app.route('/login', methods=['GET','POST'])
def login():
    if request.method=='POST':
        login=request.form.get('login')
        password=request.form.get('password')
        remember_me=request.form.get('remember_me')=='on'
        if login and password:
            cursor = mysql.connection.cursor(named_tuple=True)
            cursor.execute('SELECT * FROM users WHERE login = %s AND password_hash = SHA2(%s, 256);', (login, password))
            db_user = cursor.fetchone()
            cursor.close()
            if db_user:
                user= User(user_id = db_user.id, login = db_user.login)
                login_user(user, remember=remember_me)


                flash('Вы успешно аутентифицированны','success')
                    
                next=request.args.get('next')
                    
                return redirect(next or url_for('index')) 
        flash('Введены неверные логин и/или пароль.','danger')    
    return render_template('login.html')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/secret_page')
@login_required  # Декоратор проверяет, авторизирован ли данный пользователь в системе перед тем, как выполнять дальнейшее действие
def secret_page():
    return render_template('secret_page.html')

if __name__ == "__main__":
    app.run(debug=True)