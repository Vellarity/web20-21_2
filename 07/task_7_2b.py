# -*- coding: utf-8 -*-
"""
Задание 7.2b

Дополнить скрипт из задания 7.2a:
* вместо вывода на стандартный поток вывода,
  скрипт должен записать полученные строки в файл config_sw1_cleared.txt

При этом, должны быть отфильтрованы строки, которые содержатся в списке ignore.
Строки, которые начинаются на '!' отфильтровывать не нужно.

Ограничение: Все задания надо выполнять используя только пройденные темы.

"""

ignore = ["duplex", "alias", "Current configuration"]

f = open('config_sw1.txt','r')

temp =''
deletelist =''

for line in f:
    temp += line
    if line.startswith('!')==False:
        if any(i in line for i in ignore):
            deletelist += line
            
s1 = temp.splitlines()
s2 = deletelist.splitlines()

s3 = [line for line in s1 if line not in s2]

f2 = open('config_sw1_cleared.txt','w')

f2.write('\n'.join(s3))

f.close()
f2.close()
